#ifndef ANDROID_GUI_MOTIONEYES_MANAGER_H
#define ANDROID_GUI_MOTIONEYES_MANAGER_H

#include <stdint.h>
#include <sys/types.h>

#include <binder/IBinder.h>

#include <utils/Errors.h>
#include <utils/RefBase.h>
#include <utils/Singleton.h>
#include "IMotionEyesListener.h"

namespace android {

class IMotionEyes;

enum motion_msg_type {
    MOTION_MSG_NOP               = 0, // interface test message
    MOTION_MSG_LIGHTSCREEN      = 1,
    MOTION_MSG_KNOCK             = 2,
    MOTION_MSG_CALL              = 3,
    MOTION_MSG_WALK       = 4,
    MOTION_MSG_RUN        = 5,
    MOTION_MSG_ECG        = 6,
};

typedef enum
{
    MOTION_MODE_UNKNOWN,
    MOTION_MODE_LIGHTSCREEN,
    MOTION_MODE_KNOCK,
    MOTION_MODE_CALL,
    MOTION_MODE_WALK,
    MOTION_MODE_RUN,
    MOTION_MODE_ECG,
} MotionMode;

class MotionEyesListener
{
public:
    virtual void notify(int msg, int ext1, int ext2, const Parcel *obj) = 0;
};

class MotionEyesManager :
      public BnMotionEyesListener
{
public:
    MotionEyesManager();
    ~MotionEyesManager();

    // used by jni
    status_t setlistener(MotionEyesListener* listener,int whatListen,const char* appName);
    status_t unsetlistener(const char* appName);
    status_t registerMatcher(MotionMode mode);
    status_t unregisterMatcher(MotionMode mode);

    void pause(MotionMode mode);
    void resume(MotionMode mode);
    void reset(MotionMode mode);

    //used by MotionEyes.cpp
    void notify(int msg, int ext1, int ext2, const Parcel *obj);
private:
    // DeathRecipient interface
    void motionEyesManagerDied();
    status_t assertStateLocked() const;

private:
    mutable Mutex mLock;
    mutable sp<IMotionEyes> mMotionEyes;
    MotionEyesListener* mListener;
    mutable sp<IBinder::DeathRecipient> mDeathObserver;
};

// ----------------------------------------------------------------------------
}; // namespace android

#endif // ANDROID_GUI_MOTIONEYES_MANAGER_H
